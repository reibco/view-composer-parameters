<?php
namespace Reibco\ViewComposerParameters;

use Illuminate\Support\ServiceProvider;

class ViewComposerParametersServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->package('reibco/view-composer-parameters');

        // Register the Loose Parameter Service Provider
        $this->app->register('Reibco\LooseParameter\LooseParameterServiceProvider');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Reibco\ViewComposerParameters\ViewComposer', function ($app) {
            $config = $app['config']->get('view-composer-parameters::config');
            return new ViewComposer($app->make('Reibco\LooseParameter\ParameterManager'), $config['parameter_name']);
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }
}
