<?php
namespace Reibco\ViewComposerParameters;

use Illuminate\View\View;
use Reibco\LooseParameter\ParameterManager;

class ViewComposer
{
    protected $parameterManager;
    protected $parameterName;

    /**
     * Constructor
     *
     * @param string $parameterName
     */
    public function __construct(ParameterManager $parameterManager, $parameterName)
    {
        $this->parameterManager = $parameterManager;
        $this->parameterName = $parameterName;
    }

    /**
     * Add a piece of view composer parameter data to the view.
     *
     * @param Illuminate\View\View $view
     * @param string|array $key
     * @param mixed $value
     * @return Reibco\ViewComposerParameters\ViewComposer
     */
    public function with(View $view, $key, $value = null)
    {
        $params = $this->getData($view);

        if (is_array($key)) {
            $params = array_merge($params, $key);
        } else {
            $params[$key] = $value;
        }

        $view->with($this->parameterName, $params);

        return $this;
    }

    /**
     * Tell the view composer a parameter to expect.
     *
     * @param string $name
     * @param boolean $required
     * @param string $type
     * @param mixed $default
     * @return Reibco\ViewComposerParameters\ViewComposer
     */
    public function expect($name, $required = true, $type = null, $default = null)
    {
        if (is_array($name)) {
            foreach ($name as $params) {
                call_user_func_array(array($this, "expect"), $params);
            }
        } else {
            $this->parameterManager->registerParameter($name, $required, $type, $default);
        }

        return $this;
    }

    /**
     * Load the view composer data into the registered parameters.
     *
     * @param Illuminate\View\View $view
     * @return Reibco\ViewComposerParameters\ViewComposer
     */
    public function load(View $view)
    {
        $this->parameterManager->assignData($this->getData($view));

        return $this;
    }

    /**
     * Validate the registered parameters are set correctly.
     *
     * @return Reibco\ViewComposerParameters\ViewComposer
     */
    public function validate()
    {
        $this->parameterManager->validate();

        return $this;
    }

    /**
     * Get the assigned value of a parameter
     *
     * @param string $name
     * @return mixed
     */
    public function value($name)
    {
        return $this->parameterManager->getValue($name);
    }

    /**
     * Get the array of view composer parameter data.
     *
     * @param Illuminate\View\View $view
     * @return array
     */
    protected function getData(View $view)
    {
        $from_view = isset($view[$this->parameterName]) && is_array($view[$this->parameterName]);
        return $from_view ? $view[$this->parameterName] : [];
    }
}
