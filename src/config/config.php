<?php
return array(

    /**
     * --------------------------------------------------------------------------
     * Parameter Name
     * --------------------------------------------------------------------------
     *
     * The name of the view data key to use to hold composer specific parameters
     */

    'parameter_name' => 'composer_parameters'
);
